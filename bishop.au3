#RequireAdmin
#include <udf.au3>
#include <python_udf.au3>

Local $bishop_pid = $CmdLine[1]
$value = ""
For $arg = 2 To $CmdLine[0] ;CmdLine[0] counts all elements in list.
   $value &= $CmdLine[$arg] & ','
Next
$percents = StringTrimRight($value, 1)
Local $percentages = StringSplit($percents, ',', 2)

If $bishop_pid <> "" Then
   ActivateBishop($bishop_pid, $percentages)
EndIf
Exit

Func ActivateBishop($pid, $percentages)
   ; Variables
   $amount = len($percentages)
   $bishop_pid = getProcess($pid)

   ; Group Heal F9
   $group_heal_index = $amount
   For $percentage in $percentages
	  If $percentage <= 0 or $percentage > 90 Then
		 $group_heal_index -= 1
	  EndIf
   Next

   If $group_heal_index >= 3 Then
	  ControlSend($bishop_pid, "", "", "{F9}")
	  return 0
   EndIf

;~    ; BATTLE HEAL F5-F8
   $target = -1
   For $percentage in $percentages
	  If $percentage > 0 and $percentage < 50 Then    ; change sign
		 If $target <> -1 Then
			If $percentage < $percentages[$target] Then
			   $target = charAt($percentage, $percentages)
			EndIf
		 else
			$target = charAt($percentage, $percentages)
		 EndIf
	  EndIf
   Next
   If $target <> -1 Then
	  $skill = $target + 5
	  ControlSend($bishop_pid, "", "", '{F'&$skill&'}')
	  return 0
   EndIf

   ; NORMAL HEAL
   $target = -1
   For $percentage in $percentages
	  If $percentage > 0 and $percentage < 80 then    ; change sign
		 If $target <> -1 Then
			If $percentage < $percentages[$target] Then
			   $target = charAt($percentage, $percentages)
			EndIf
		 else
			$target = charAt($percentage, $percentages)
		 EndIf
	  EndIf
   Next
   If $target <> -1 Then
	  $skill = $target + 1
	  ControlSend($bishop_pid, "", "", '{F'&$skill&'}')
	  return 0
   EndIf
   ControlSend($bishop_pid, "", "", "{F11}")
EndFunc