#include <Array.au3>
#include <FileConstants.au3>
#include <dictionary.au3>
#include <python_udf.au3>
#include <udf.au3>

Local $table[] = [5,9,11,2,32,12]
Local $app[][] = [["key1",$table], ["key2",3]]

$dict = dict()

dict_array_2D($dict, $app)

dict_update_array($dict, "key1", 1, "sweethome")

;~ MsgBox(0, "", _ArrayMax($table))
Local $hFileOpen = FileOpen("nowy.txt", 2)
FileWrite($hFileOpen, "Line 2")
FileWrite($hFileOpen, "This is still line 2 as a new line wasn't appended to the last FileWrite call." & @CRLF)
FileWrite($hFileOpen, "Line 3" & @CRLF)
FileWrite($hFileOpen, "Line 4")


FileClose($hFileOpen)

$file = read_file("nowy.txt")

For $line in $file
   print($line)
Next

$dict = null
