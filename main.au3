#RequireAdmin
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ColorConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ProgressConstants.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <Array.au3>
#include <math.au3>
#include <Misc.au3>
#include <udf.au3>
#include <health_bar.au3>
#include <python_udf.au3>
#include <dictionary.au3>

Local $dict_pid = dict()
Local $dict_states = dict()

Local $dict_spoil_keys = dict()
dict_add($dict_spoil_keys, "single_attack", "{F2}")
dict_add($dict_spoil_keys, "next_target", "{F3}")
dict_add($dict_spoil_keys, "assist", "{F3}")
dict_add($dict_spoil_keys, "follow", "{F4}")
dict_add($dict_spoil_keys, "attack", "{F5}")
dict_add($dict_spoil_keys, "spoil", "{F6}")
dict_add($dict_spoil_keys, "sweep", "{F7}")






Local $dict_bountyHunter = dict()
dict_add($dict_bountyHunter, "MP", 0)

Local $process_pool[12]
Local $hotkey_pool[12]
Local $skill_pool[12]
Local $status_pid[6]
$WINDOW_TITLE = "ESTE"
$WINDOW_WIDTH = 550
$WINDOW_HEIGHT = 350
$WINDOW_POSX = 200
$WINDOW_POSY = 200

$OPTION_SCAN = "Scan"
$OPTION_PROCESS_TEXT = "L2.exe"
$OPTION_SHOW = "Check Window"
$OPTION_CLEAR = "Clear list"
$REGISTERED_BISHOP = 0
$RUN_KEY_MAPPING = 0
$CRAFTING = 0
$SPOIL = 0
$REGISTERED_SPOILER = 0


$form = GUICreate($WINDOW_TITLE, $WINDOW_WIDTH, $WINDOW_HEIGHT)
   GUISetBkColor('0x03477F')
    $process_name = GUICtrlCreateCombo($OPTION_PROCESS_TEXT, 20, 20, 77, 17)
	GUICtrlSetData(-1, 'L2.bin')
	modify_combos(-1)
    $process_list = GUICtrlCreateList("", 100, 20, 77, 43)
    $process_show = GUICtrlCreateButton($OPTION_SHOW, 180, 20, 77, 17)
   modify_buttons(-1)


    $process_scan = GUICtrlCreateButton($OPTION_SCAN, 20, 40, 77, 17)
   modify_buttons(-1)
    $process_clear = GUICtrlCreateButton($OPTION_CLEAR, 180, 40, 77, 17)
   modify_buttons(-1)

    For $i = 0 To 11 Step 1
        $process_pool[$i] = GuiCtrlCreateCombo("", 20, 60 + $i*20, 57, 17)
		modify_combos(-1)
        $hotkey_pool[$i] = GuiCtrlCreateInput("", 100, 60 + $i*20, 57, 17)
		modify_combos(-1)
        $skill_pool[$i] = GuiCtrlCreateCombo("", 180, 60 + $i*20, 57, 17)
		modify_combos(-1)
	 Next
	 For $i=0 To 5 Step 1
	    $status_pid[$i] = GUICtrlCreateCombo("", 480, 10 + 45 * $i, 50, 17)
		modify_combos(-1)
     Next

    $process_start = GUICtrlCreateButton("IDLE", 50, 310, 157, 17)
	modify_buttons(-1)
	$craft_start = GUICtrlCreateButton("craft", 210, 310, 50, 17)
	modify_buttons(-1)
	$craft_heal = GUICtrlCreateButton("mouse heal", 270, 310, 70, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])
	$craft_craft = GUICtrlCreateButton("mouse craft", 270, 330, 70, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])

   $spoil_start = GUICtrlCreateButton("start spoil", 380, 290, 70, 17)
   modify_buttons(-1)
   $input_follow = GUICtrlCreateInput("", 380, 310, 70, 17)
   modify_combos(-1)
   $bounty_hunter = GUICtrlCreateInput("", 380, 330, 70, 17)
   modify_combos(-1)

	$activate_bishop = GUICtrlCreateButton("Bishop", 20, 330, 140, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])

	$activate_bishop_pid = GUICtrlCreateInput("", 180, 330, 50, 17)
	$pick_bishop_pid = GUICtrlCreateButton('pick', 230, 330, 30, 17)
	modify_buttons(-1)
	  GUICtrlSetBkColor(-1, $BLUE[0])

	 For $i = 0 To 11 Step 1
		GUICtrlSetData($skill_pool[$i], 'F1|F2|F3|F4|F5|F6|F7|F8|F9|F10|F11|F12')
     Next
     GUISetState(@SW_SHOW)

searchPid()

Local $heal_x = 0
Local $heal_y = 0
Local $craft_x = 0
Local $craft_y = 0
Local $memoryReadTimeOut = 0
getWindowMemoryAddress($dict_pid)
getBars($dict_pid, $dict_states)
dict_print($dict_pid)
While 1
   If $memoryReadTimeOut > 15 Then

	  getBars($dict_pid, $dict_states)
	  $memoryReadTimeOut = 1
   Else
	  $memoryReadTimeOut += 1
   EndIf

    $listener = GUIGetMsg()
	lookAtParams()
    Switch $listener
        Case $GUI_EVENT_CLOSE
            Exit

         Case $process_scan
		    searchPid()

		 Case $process_show
			checkWindow()

		 Case $spoil_start
			If $REGISTERED_SPOILER == 0 and GUICtrlRead($bounty_hunter) <> "" Then
			   $REGISTERED_SPOILER = 1
			   modify_gui($spoil_start, $BLUE[2])
			   AdlibRegister("prepareBountyHunter")
			Else
			   $REGISTERED_BISHOP = 0
			   modify_gui($spoil_start, $BLUE[0])
			   mbox("Unregistered manually.")
			   AdlibUnRegister("prepareBountyHunter")
			EndIf

		 Case $activate_bishop
			If $REGISTERED_BISHOP == 0 and GUICtrlRead($activate_bishop_pid) <> "" Then
			   $REGISTERED_BISHOP = 1
			   modify_gui($activate_bishop, $BLUE[2])
			   AdlibRegister("prepareBishop")
			Else
			   $REGISTERED_BISHOP = 0
			   modify_gui($activate_bishop, $BLUE[0])
			   mbox("Unregistered manually.")
			   AdlibUnRegister("prepareBishop")
			EndIf

		 Case $pick_bishop_pid
			$pick = GUICtrlRead($process_list)
			If $pick <> "" Then GUICtrlSetData($activate_bishop_pid, $pick)

		 Case $craft_start
			$delay = 100
			If $craft_x <> 0 and $craft_y <> 0 and $heal_x <> 0 and $heal_y <> 0 and GUICtrlRead($status_pid[0]) <> "" Then
			   getBars($dict_pid, $dict_states)
			   $first_pid = Number(GUICtrlRead($status_pid[0]), 3)
			   WinActivate(getProcess($first_pid))
			   $dll = DllOpen("user32.dll")
			   While 1
				  $bar_mp = $dict_states($first_pid)[1]
				  If GUICtrlRead($bar_mp) > 10 Then
					 MouseClick("", $craft_x, $craft_y)
				  Else
					 MouseClick("", $heal_x, $heal_y)
				  EndIf
				  If _IsPressed("1B", $dll) Then
					 ExitLoop
				  EndIf
				  getBars($dict_pid, $dict_states)
				  Sleep($delay)
			   WEnd
			   DllClose($dll)
			Else
			   mbox("No coords for buffer and craft window")
			EndIf

		 Case $craft_craft
			pick_coords("crafter", GUICtrlRead($status_pid[0]))

		 Case $craft_heal
			pick_coords("buffer", GUICtrlRead($status_pid[0]))

		 Case $process_start
			If $RUN_KEY_MAPPING == 0 Then
			   $RUN_KEY_MAPPING = 1
			   modify_gui($process_start, $BLUE[2], "RUNNING...")
			   For $key in $hotkey_pool
				  HotKeySet('{'&GUICtrlRead($key)&'}', "Command")
			   Next
			Else
			   $RUN_KEY_MAPPING = 0
			   modify_gui($process_start, $BLUE[1], "IDLE")
			   For $key in $hotkey_pool
				  HotKeySet('{'&GUICtrlRead($key)&'}')
			   Next
			EndIf

		 Case $process_clear
			GUICtrlSetData($process_list, "")
    EndSwitch
    Sleep(1)
 WEnd

Func pick_coords($handler, $pid)
   If $pid <> "" Then
	  $win_handler = getProcess($pid)
   Else
	  MsgBox(0, "", "no pid")
	  Return 0
   EndIf
   WinActivate($win_handler)
   $dll = DllOpen("user32.dll")
   While 1
	  $mPos = MouseGetPos()
	  If _isPressed("01", $dll) and $handler == "buffer" Then
		 $heal_x = $mPos[0]
		 $heal_y = $mPos[1]
		 ExitLoop
	  ElseIf _IsPressed("01", $dll) and $handler == "crafter" Then
		 $craft_x = $mPos[0]
		 $craft_y = $mPos[1]
		 ExitLoop
	  EndIf
	  Sleep(1)
   WEnd
   DllClose($dll)
   Sleep(500)
   WinActivate($WINDOW_TITLE)
EndFunc

Func prepareBishop()
   AdlibUnRegister("prepareBishop")
   $bishop_pid = GUICtrlRead($activate_bishop_pid)
   If $bishop_pid <> "" Then
	  Local $parameters = $bishop_pid

	  For $gui_pid in $status_pid
		 $key = Number(GUICtrlRead($gui_pid), 3)
		 If $dict_states.exists($key) Then
			$status_bar_hp = $dict_states.item($key)[0]
			$parameters &= ' ' & GUICtrlRead($status_bar_hp)
		 EndIf
	  Next

	  run_au3("bishop.au3", $parameters)
   EndIf
   AdlibRegister("prepareBishop", 2000)
EndFunc

Func run_au3($sFilePath, $parameters = "", $sWorkingDir = "", $iShowFlag = @SW_SHOW, $iOptFlag = 0)
    Return Run('"' & @AutoItExe & '" /AutoIt3ExecuteScript "' & $sFilePath & '" ' & $parameters, $sWorkingDir, $iShowFlag, $iOptFlag)
 EndFunc

Func getWindowMemoryAddress($dict)
   Local $window_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x678]
   For $pid in $dict.keys
	  $handler = analize_memory($pid, $window_offsets)
	  dict_update($dict, $pid, $handler)
   Next
EndFunc

Func prepareBountyHunter()
;~    If $dict_bountyHunter.item("Follow_HP") == 0 Then
;~ 	  Local $pid_follow = GUICtrlRead($input_follow)
;~ 	  Local $maxHP_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x670]
;~ 	  $follow_hp = scan_status($dict_pid.item($pid_follow), $pid_follow, $maxHP_offsets)
;~ 	  dict_add($dict_bountyHunter, "Follow_HP", $follow_hp)
;~    EndIf

   AdlibUnRegister("prepareBountyHunter")
   Local $read_pid = GUICtrlRead($bounty_hunter)
   Local $pid = Number($read_pid, 3)
   Local $target_currentHP[] = [0x7B8, 0x760, 0x510, 0x264, 0x5DC]
   Local $target_maxHP[] = [0x7B8, 0x760, 0x510, 0x264, 0x5D8]

   Local $currentMP_offsets[] = [0x594, 0x328, 0x444, 0xDC, 0x678]

   If $dict_pid.exists($pid) Then
	  $core = $dict_pid.item($pid)
	  $target_current_hp = scan_status($core, $pid, $target_currentHP, 0x0039CCA4)
	  $target_max_hp = scan_status($core, $pid, $target_maxHP, 0x0039CCA4)
	  $valueMP = scan_status($core, $pid, $currentMP_offsets)
	  $percent = Round(100 * $target_current_hp / $target_max_hp, 0)
	  $value2MP = $dict_bountyHunter.item("MP")
	  $compare = $value2MP - $valueMP
	  Local $command = ""


	  If $target_current_hp > 0 Then
		 If $percent > 0 and $SPOIL == 1 Then
			$command = "single_attack"
;~ 		 ElseIf $compare < -5 Then
;~
;~ 			$command = "single_attack"
		 ElseIf $percent >= 50 and $SPOIL == 0 Then
			$command = "spoil"
			$SPOIL = 1
		 EndIf
	  ElseIf $SPOIL == 1 and $target_current_hp == 0 Then
		 $command = "sweep"
		 $SPOIL = 0
	  Else
		 $command = "follow"
	  EndIf
	  ConsoleWrite($target_current_hp)
	  dict_update($dict_bountyHunter, "MP", $valueMP)

	  Local $hotkey = $dict_spoil_keys.item($command)
	  Local $process = getProcess($read_pid)
	  ControlSend($process, "", "", $hotkey)


;~ 	  Local $parameters = $pid & ' ' & $target_current_hp & ' ' & $percent & ' ' & $command
;~ 	  run_au3("bountyHunter.au3", $parameters)
   EndIf
   AdlibRegister("prepareBountyHunter", 500)
EndFunc

Func getBars($dict, $states)

   Local $maxHP_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x670]
   Local $currentHP_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x678]

   Local $maxMP_offsets[] = [0x594, 0x328, 0x444, 0xDC, 0x670]
   Local $currentMP_offsets[] = [0x594, 0x328, 0x444, 0xDC, 0x678]

   $index = 0
   For $pid in $status_pid
	  $element = ""
	  $core = "" ;Handler to 'NWindow.DLL'
	  $val = Number(GUICtrlRead($pid), 3) ; variable type: Double
	  If $val <> "" and $dict.exists($val) Then
		 $element = $val
		 $core = $dict.item($val)
	  ElseIf $val <> "" and $states.exists($val) and (not $dict.exists($val)) Then
		 dict_remove_keys($states, $val)
		 MsgBox(0, "", "usunieto", 1)
	  EndIf

	  If $element <> "" and $core <> "" Then
		 $current_hp = scan_status($core, $element, $currentHP_offsets)
		 $max_hp = scan_status($core, $element, $maxHP_offsets)
		 $percentage_hp = Round((100 * $current_hp) / $max_hp, 0)

		 $current_mp = scan_status($core, $element, $currentMP_offsets)
		 $max_mp = scan_status($core, $element, $maxMP_offsets)
		 $percentage_mp = Round((100 * $current_mp) / $max_mp, 0)

		 If not $states.exists($element) Then
			$status_bar_hp = createProgressBar(270, 10 + 45 * $index, 200, 12, 0xFF0000)
			$status_bar_mp = createProgressBar(270, 25 + 45 * $index, 200, 12, $BLUE[0])
			GUICtrlSetData($status_bar_hp, $percentage_hp)
			GUICtrlSetData($status_bar_mp, $percentage_mp)
			Local $state[] = [$status_bar_hp, $status_bar_mp]
			dict_add($states, $element, $state)
		 Else
			GUICtrlSetData($states($element)[0], $percentage_hp)
			GUICtrlSetData($states($element)[1], $percentage_mp)
		 EndIf

		 $index = $index + 1
;~ 		 Local $state[] = [$current_hp, $current_mp, $max_hp, $max_mp, $status_bar_hp, $status_bar_mp, $percentage_hp, $percentage_mp]

	  EndIf
   Next
EndFunc

Func checkWindow()
   Local $spr = GetProcess(GUICtrlRead($process_list))
   If $spr <> "" Then
	  WinActivate($spr)
	  sleep(500)
	  WinActivate($WINDOW_TITLE)
   Else
	  MsgBox(0, "Error", "Process is not selected!")
   EndIf
EndFunc

Func lookAtParams()
   For $key in $hotkey_pool
	  If GUICtrlRead($key) <> "" Then
		 Local $index = getIndex(GUICtrlRead($key), $hotkey_pool)
		 If $index >= 0 and GUICtrlRead($process_pool[$index]) == "" Then
			GUICtrlSetData($process_pool[$index], GUICtrlRead($process_list))
		 EndIf
		 If $index >= 0 and GUICtrlRead($skill_pool[$index]) == "" Then
			GUICtrlSetData($skill_pool[$index], 'F' & GUICtrlRead($key))
		 EndIf
	  EndIf
   Next
EndFunc

Func searchPid()
   dict_array_2D($dict_pid, ProcessList(GUICtrlRead($process_name)), 1, 1)

   For $pid in $dict_pid.keys
	  GUICtrlSetData($process_list, $pid)

	  For $control_id in $process_pool
		 GUICtrlSetData($control_id, $pid)
	  Next

	  For $control_id in $status_pid
		 GUICtrlSetData($control_id, $pid)
	  Next
   Next
EndFunc

Func Command()
   Local $key = StringTrimRight(StringTrimLeft(@HotKeyPressed, 1), 1)
   Local $index = getIndex($key, $hotkey_pool)
   If $index >= 0 Then
	  makeCommand($index)
   EndIf
EndFunc

Func makeCommand($arg)
   Local $process = GUICtrlRead($process_pool[$arg])
   Local $hotkey = GUICtrlRead($hotkey_pool[$arg])
   Local $skill = GUICtrlRead($skill_pool[$arg])

   if $process <> 0 and $hotkey <> "" and $skill <> "" Then
		 Local $hwnd = getProcess($process)
		 ControlSend($hwnd, "", "", '{'&$skill&'}')
	  EndIf
EndFunc


