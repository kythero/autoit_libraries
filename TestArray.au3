#include <Array.au3>
#include <python_udf.au3>
#include <dictionary.au3>



$dict = dict()
$dict.CompareMode = 1

$dict.add("One", 0)
$dict.add("Ten", "2")


$dict = null

Local $test[] = [1]
Local $val = 1

For $el in $val
   print($el)
Next


Func printKeys($dict)
   For $key in $dict
	  print($key)
   Next
EndFunc

Func printValues($dict)
   For $key in $dict
	  print($dict.item($key))
   Next
EndFunc

Func add($dict, $key, $value)
   If $dict.exists($key) Then
	  $dict.remove($key)
	  $dict.add($key, $value)
   Else
	  $dict.add($key, $value)
   EndIf
EndFunc