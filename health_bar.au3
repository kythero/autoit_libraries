#include-once
#include <NomadMemory.au3>
#include <python_udf.au3>
;~ main() ; check PID only.
Func main()
   $pid = "6380"
   Local $maxHP_offsets[] = [0x728, 0x1B4, 0x7B4, 0x584, 0x670]
   Local $currentHP_offsets[] = [0x728, 0x1B4, 0x7B4, 0x584, 0x678]
   $current_hp = scan_status($pid, $currentHP_offsets)
   $max_hp = scan_status($pid, $maxHP_offsets)

   MsgBox(0, "maxHP: " & $current_hp, "Current hp: " + $max_hp)
EndFunc


Func analize_memory($pid, $window_offsets)
   $memory = _MemoryOpen(ProcessExists($pid))
   $count = 0
   Local $possibility[] = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
;~    Local $window_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x678]
   $wartosc = 9460301
   $read_value = ""
   $pattern = ""
   For $element1 in $possibility
	  For $element2 in $possibility
		 For $element3 in $possibility
			$pattern = '0x0'&$element1&$element2&$element3&'0000'
			$read_value = _MemoryRead($pattern, $memory, 'dword')

			If $read_value == $wartosc Then
			   $step = $pattern + 0x00380F80
			   For $offset in $window_offsets
				  $step = _MemoryRead($step, $memory, 'dword') + $offset
			   Next
			   $step = _MemoryRead($step, $memory, 'dword')
			   If $step > 1000 Then
				  ExitLoop 3
			   EndIf
			EndIf
			$count += 1
		 Next
	  Next
   Next
   _MemoryClose($memory)
   return $pattern
EndFunc

Func scan_status($core, $pid, $offsets, $static_offset = 0x00380F80)
   ; Core.dll = 0x10100000
   ; NWindow.DLL = 0x0AC60000
   ; Core.dll static offset: 0x0013C698
   ; NWindow static offset: 0x00381140
;~    $core = 0x10100000
;~    $static_offset = 0x00380F80
;~    Local $window_offsets[] = [0x594, 0x328, 0x444, 0x114, 0x678]
   $memory = _MemoryOpen(ProcessExists($pid))
	  $value = $core + $static_offset
	  For $offset in $offsets
		 $value = _MemoryRead($value, $memory, 'dword') + $offset
	  Next
	  $value = _MemoryRead($value, $memory, 'dword')
   _MemoryClose($memory)

   return $value
EndFunc

Func _GetHwndFromPID($PID)
	$hWnd = 0
	$winlist = WinList()
	Do
		For $i = 1 To $winlist[0][0]
			If $winlist[$i][0] <> "" Then
				$iPID2 = WinGetProcess($winlist[$i][1])
				If $iPID2 = $PID Then
					$hWnd = $winlist[$i][1]
					ExitLoop
				EndIf
			EndIf
		Next
	Until $hWnd <> 0
	Return $hWnd
 EndFunc;==>_GetHwndFromPID




 ;===================================================================================================
; Function........:  _MemoryGetBaseAddress($ah_Handle, $iHD)
;
; Description.....:  Reads the 'Allocation Base' from the open process.
;
; Parameter(s)....:  $ah_Handle - An array containing the Dll handle and the handle of the open
;                                 process as returned by _MemoryOpen().
;                    $iHD - Return type:
;                       |0 = Hex (Default)
;                       |1 = Dec
;
; Requirement(s)..:  A valid process ID.
;
; Return Value(s).:  On Success - Returns the 'allocation Base' address and sets @Error to 0.
;                    On Failure - Returns 0 and sets @Error to:
;                       |1 = Invalid $ah_Handle.
;                       |2 = Failed to find correct allocation address.
;                       |3 = Failed to read from the specified process.
;
; Author(s).......:  Nomad. Szhlopp.
; URL.............:  http://www.autoitscript.com/forum/index.php?showtopic=78834
; Note(s).........:  Go to Www.CheatEngine.org for the latest version of CheatEngine.
;===================================================================================================
Func _MemoryGetBaseAddress($ah_Handle, $iHexDec = 0)

    Local $iv_Address = 0x00100000
    Local $v_Buffer = DllStructCreate('dword;dword;dword;dword;dword;dword;dword')
    Local $vData
    Local $vType

    If Not IsArray($ah_Handle) Then
        SetError(1)
        Return 0
    EndIf


    DllCall($ah_Handle[0], 'int', 'VirtualQueryEx', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer))

    If Not @Error Then

        $vData = Hex(DllStructGetData($v_Buffer, 2))
        $vType = Hex(DllStructGetData($v_Buffer, 3))

        While $vType <> "00000080"
            DllCall($ah_Handle[0], 'int', 'VirtualQueryEx', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer))
            $vData = Hex(DllStructGetData($v_Buffer, 2))
            $vType = Hex(DllStructGetData($v_Buffer, 3))
            If Hex($iv_Address) = "01000000" Then ExitLoop
            $iv_Address += 65536

        WEnd

        If $vType = "00000080" Then
            SetError(0)
            If $iHexDec = 1 Then
                Return Dec($vData)
            Else
                Return $vData
            EndIf

        Else
            SetError(2)
            Return 0
        EndIf

    Else
        SetError(3)
        Return 0
    EndIf

EndFunc