#include-once
#include <StringConstants.au3>


Func len($type)
   if VarGetType($type) == "Array" Then
	  Local $index = 0
	  For $el in $type
		 $index += 1
	  Next
	  return $index
   ElseIf VarGetType($type) == "String" Then
	  return StringLen($type)
   EndIf
EndFunc


;~ == re_match($pattern, $text) ============================
;~ =========================================================
;~  Example:
;~    Local $text = "Home"
;~    If re_match(".ome", $text) Then
;~ 		 ConsoleWrite($text)
;~    EndIf
;~ =========================================================
Func re_match($pattern, $text)
   ; 0 - $STR_REGEXMATCH
   ; return true or false
   $result = StringRegExp($text, $pattern, 0)
   If $result <> 2 Then
	  return $result
   Else
	  return @error
   EndIf
EndFunc


;~ == re_search($pattern, $text, $flag_boolean = false) ====
;~ =========================================================
;~  Example:
;~    Local $text = '<test>a</test> <test>b</test> <test>c</Test>'
;~    Local $array_results = re_search('<test>(.*?)</test>(.*?)</Test>', $text)
;~    For $element in $array_results
;~ 		 ConsoleWrite($element & @CRLF)
;~    Next
;~ =========================================================
Func re_search($pattern, $text, $flag_boolean = false)
   ; 1 - $STR_REGEXPARRAYMATCH
   ; 2 - $STR_REGEXPARRAYFULLMATCH
   Local $flag = 1
   If $flag_boolean Then $flag = 2

   Local $matched = StringRegExp($text, $pattern, $flag)
   If $matched == 0 or $matched == 1 or $matched == 2 Then
	  return 0
   Else
	  return $matched
   EndIf
EndFunc


;~ == re_findAll($pattern, $text, $flag_boolean = false) ===
;~ =========================================================
;~ Example:
;~    Local $array_results = re_findAll(".om", "Home, sweet home")
;~    If $array_results <> 0 Then
;~ 		 For $element in $array_results
;~ 			ConsoleWrite($element & @CRLF)
;~ 		 Next
;~    EndIf
;~ =================================================
Func re_findAll($pattern, $text, $flag_boolean = false)
   ; 3 - $STR_REGEXPARRAYGLOBALMATCH
   ; 4 - $STR_REGEXPARRAYGLOBALFULLMATCH
   Local $flag = 3
   If $flag_boolean Then $flag = 4

   Local $matched = StringRegExp($text, $pattern, $flag)
   If $matched == 0 or $matched == 1 or $matched == 2 Then
	  return 0
   Else
	  return $matched
   EndIf
EndFunc